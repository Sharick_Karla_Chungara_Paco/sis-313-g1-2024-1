import Image from "next/image";
import styles from "./page.module.css";
import Header from "@/app/Components/Header";
import Title from "@/app/Components/Title";
import Formulario from "@/app/Components/Formulario";
import Button from "@/app/Components/Button";
import Link from 'next/link';


export default function Home() {
  return (
    <main>
<section className="SignUp">
<div>
    <Header imageUrl="/Images/Logo.svg" title="Your Logo "></Header>
</div>

<div className="Container-formulario">
    <div className="img-secundary">
        <Image className="image-mala" src="/Images/Image-secundary.svg" alt="" width={100} height={ 100} />
     </div>
     <div className="Container-form2">
    <Title  title="Sign Up" paragraph="Let’s get you all st up so you can access your personal account."></Title>
    <div className="Datos">
    <div className="form1">
        <Formulario name="First Name" text="email"></Formulario>
        <Formulario name="Last Name" text="email"></Formulario>
    </div>
    <div className="form2">
        <Formulario name="Email" text="email"></Formulario>
        <Formulario name="Phone number" text="email"></Formulario>
    </div>
    <div className="form3">
        <Formulario name="Password" text="password"></Formulario>
        <Formulario name="Confirm Password" text="email"></Formulario>
    </div>
    <div className="Marcador">
          <input type=" " />
          <h6>I agree to all the Terms and Privacy Policies</h6>
    </div>
        <Button text="Create accout"></Button>
    <div className="Link1">
         <h5>Already have an account? Login</h5>
         <Link href="/" >Login</Link>
    </div>
      <Image  className="texto-img" src="/Images/img-text.svg" alt="" width={100} height={ 100}/>
  
  <div className="container-iconos" >
 
        <div>
        <Image src="/Images/Face.svg" alt="" width={100} height={ 100}/>
        </div>
        <div>
        <Image src="/Images/Google.svg"alt="" width={100} height={ 100}/>
        </div>
        <div>
        <Image src="/Images/Apple.svg" alt="" width={100} height={ 100}/>
        </div>
  </div>
</div>
</div>
</div>
</section>

</main>
);
}
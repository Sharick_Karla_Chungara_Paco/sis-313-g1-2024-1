import Image from "next/image";
import styles from "./page.module.css";
import Header from "./Components/Header";
import Title from "./Components/Title";
import Formulario from "./Components/Formulario";
import Button from "./Components/Button";
import Link from 'next/link';



export default function Home() {
  return (
    <main>
      <section className="Login">
        <div className="Container-Logo">
          <Header imageUrl="/Images/Logo.svg" title="Your Logo "></Header>
        </div>
        <div className="Caja-principal">
          <div className="Container-form">
                 <Title title="Login" paragraph="Login to access your travelwise  account"></Title>
             <div className="Formulario">
                  <Formulario name="Email" text="email"></Formulario>
                  <Formulario name="Password" text="password"></Formulario>
             <div className="Marcador">
                  <input type=" " />
                  <h6>Remember me</h6>
                  <h6 className="Password-rosa">Forgot password</h6>
            </div>
                  <Button text="Login"></Button>
            <div className="Link1">
                 <h5>Don’t have an account? </h5>
                 <Link href="/SignUp" >Sign up</Link>
            </div>
                 <Image className="texto-img" src="/Images/img-text.svg" alt=""width={100} height={ 100} />
                 <div className="container-iconos" >
                  <div>
                  <Image src="/Images/Face.svg" alt=""width={100} height={ 100}/>
                  </div>
                  <div>
                  <Image src="/Images/Google.svg"alt="" width={100} height={ 100}/>
                  </div>
                  <div>
                 <Image src="/Images/Apple.svg"alt="" width={100} height={ 100} />
                </div>
              </div>
            </div>
          </div>
            <div className="Image">
              <Image src="/Images/Image-principal.svg" alt=""width={100} height={ 100}  />
            </div>
        </div>  
      </section>
      
    </main>
  );
}

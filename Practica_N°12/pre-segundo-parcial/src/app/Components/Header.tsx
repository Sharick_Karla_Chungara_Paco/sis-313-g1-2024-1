import Image from "next/image";
export default function Header(props:{imageUrl:any,title:string}){
    return(
        <div className=" header-logo">
            <Image className="Logo"src={props.imageUrl}alt="" width={100} height={ 100}/>
            <h1>{props.title}</h1>

        </div>
      
    )

}
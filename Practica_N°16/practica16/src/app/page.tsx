"use client"

import Image from "next/image";
import styles from "./page.module.css";
import * as React from 'react';
import Button from '@mui/material/Button';
import CardService from "@/components/CardService";
import { getPokemon } from '@/services/pokemon.service';
import { usePokemon } from '@/hooks/usePokemon';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { themePersonal } from '@/helpers/theme';

const centerFlex = { display: "flex", justifyContent: "center", alignItems: "center" }
const flexDirectionColumn = { flexDirection: "column" }


export default function Home() {
  const { pokemon, loading} = usePokemon()
  console.log(loading?'CARGANDO...': '')
 if(!loading){
  console.log(pokemon)
 }
  return (
    <main>
      <Box sx={{ display: 'flex' }}>
      <Box component="main" sx={{ p: 3, width: "100%" }}>
      <Grid item xs={6} sx={{ ...centerFlex, ...flexDirectionColumn }}>
                <Typography className="Title-header" variant="h1" gutterBottom sx={{fontWeight:"bold"}}>
                  Pokemon -PokeApi
                </Typography>
                <img className="pikachu"src="/Images/pikachu.png" alt="" />
                <Box>
                  <Typography variant="h6" gutterBottom className='paragraph border-left'>
                  The EuroLeague Finals Top Scorer is the individual award for the player that gained the highest points in the EuroLeague Finals
                  </Typography>
                </Box>
                <Box sx={{ ...centerFlex, justifyContent: 'space-between', width: '100%', marginTop:"20px" }}>
               
                </Box>
        </Grid>
   




      <Grid container spacing={2} sx={{marginY:"40px"}}>
              <Grid item xs={12}>
              <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                  <Typography variant="h4" gutterBottom className='pagination-item'>
                    Pokemones
                  </Typography>
                
                </Box>
              </Grid>
              {pokemon?.results.map((item: any, index: number) => {
                const splitUrl = item.url.split('/')
                const id = splitUrl[splitUrl.length - 2]
                return (
                  <Grid key={index} item xs={12} sm={12} md={6} lg={3} xl={3}>
                    <CardService
                      avatar={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
                      name={item.name}
                      image={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
                      date='04 June 2023'
                      title={item.title}
                      parrafo={item.parrafo}>
                    </CardService>
                  </Grid>
                )
              })}
            </Grid>
            </Box>
            </Box>
      
    </main>
  );
}



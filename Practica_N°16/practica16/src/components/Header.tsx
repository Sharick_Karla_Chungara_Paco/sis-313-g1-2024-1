"use client"
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

interface Props { 
  window?: () => Window;
}
const drawerWidth = 240;
const navItems = ['Home', 'Category', 'Trending News','Recent News','Clubs Ranking', 'Sports Article' ];

export default function Home(props: Props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
      <img src="/images/Logo.svg" alt="" />
      </Typography>
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBar component="nav" sx={{ backgroundColor: "transparent", height:"92px", justifyContent:"center" }}>
                    <Toolbar sx={{ display: "flex", justifyContent: "space-evenly", color: "#000", }}>
                        <Typography
                            variant="h6"
                            component="div"
                        >
                          <img src="/images/Logo.svg" alt="" />
                        </Typography>
                        <Box>
                            {navItems.map((item) => (
                                <Button key={item} sx={{ color: '#26262699',marginRight:'50px',  fontSize:'15px' }}>
                                    {item}
                                </Button>
                            ))}
                            <Button sx={{backgroundColor:"#B8C2CE", color:"white", border:"none"}} variant="outlined" startIcon={<SearchIcon />}>
                                <i className='icon-search' />Search
                            </Button>
                        </Box>
                    </Toolbar>
                </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
      <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
      
      </Box>
    </Box>
            
  );
}

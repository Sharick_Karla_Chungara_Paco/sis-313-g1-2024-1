import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { Box } from '@mui/material';

interface CardServiceProps {
    avatar: string;
    image: string;
    name: string;
    date: string;
    title:string;
    parrafo:string
}

export default function CardService({ avatar, image, name, date, title, parrafo }: CardServiceProps) {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        
        <Card sx={{ maxWidth: 300, paddingX: "50px", paddingY: "10px", height: "100%" }}>
            <CardMedia
                component="img"
                height="195"
                image={image}
                alt="Paella dish"
            />
            <CardContent sx={{height:"150px",display:"flex",flexDirection:"column",justifyContent:"space-between"}}>
                
                <Box sx={{ display: "flex",width:"100%", alignItems: "end"}}>
                    <Box sx={{display: "flex", flexDirection:"column"}}>
                        <Avatar sx={{ marginRight: "10px" }} aria-label="recipe" src={avatar}>
                       
                        </Avatar>
                        <Box>
                            <Typography variant="body2" color="text.secondary">
                                {name}
                            </Typography>
                            <Typography variant="body2" color="text.secondary" className="truncate">
                                {date}
                            </Typography> 
                        </Box>
                        <Box sx={{display:"flex", flexDirection:"column"}}>
                        <Typography variant="h6">
                               {title}
                        </Typography>
                        <Typography variant="body2" gutterBottom>
                               {parrafo}
                        </Typography>

                        </Box>
                    </Box>
                </Box>
            </CardContent>
        </Card>
    );
}

import Image from "next/image";
import styles from "./page.module.css";
import Button from "./components/Button";
import MewPoke from "./components/MewPoke";
import Videos from "./components/Videos";
import PlanesSuscripcion from "./components/PlanesSuscripcion";


export default function Home() {
  return (
    <main>
      <h1> 🌻PREGUNTA-1🌻</h1>
      <div className="container-Botoncitos">
        <Button title="HOLA"></Button>
        <Button title="ADIOS"transparent></Button>
      </div>
      <h1> ☘️PREGUNTA-2☘️</h1>
      <div className="container-Pokemones">
        <MewPoke title="Mew"imageUrl="./images/mew-pokemon.jpeg"type="Psiquico"></MewPoke>
        <MewPoke title="Vaporeon"imageUrl="./images/Vaporeon.png"type="Eléctrico/Dragón"></MewPoke> 
        <MewPoke title="Ampharos"imageUrl="./images/Ampharos.jpg"type="Agua"></MewPoke>
        <MewPoke title="Cubone"imageUrl="./images/Cubone.png"type="Tierra"></MewPoke>

      </div>  
      <h1> 🌼PREGUNTA-3🌼</h1>
      <div className="container-Videos">
        <Videos title="Riptipe 💔" videosUrl="/videos/Vance Joy  Riptide Official Video_480p.mp4" type="Vance Joy" ></Videos>
        <Videos title="Eagles  Hotel California🏨" videosUrl="/videos/Eagles  Hotel California Live 1977 Official Video HD_360p.mp4" type="Eagles" ></Videos>
        <Videos title="Can I Call You Tonight ☎️" videosUrl="/videos/Dayglow  Can I Call You Tonight Official Video_480p.mp4" type="Dayglow" ></Videos>
        <Videos title="Mr Blue Sky 📼" videosUrl="/videos/GUARDIANS OF THE GALAXY 2  Mr Blue Sky By Electric Light Orchestra Lyrics_480p.mp4" type="Electric Light Orchestra" ></Videos>
      </div>
      <h1> 🍁PREGUNTA-4🍁</h1>
      <div className="container-Planes">
        <PlanesSuscripcion imageUrl="./images/plan-img.svg" title="Free Plan" imgUrl="./images/revisado.svg" option="Unlimited Bandwitc" option2="Encrypted Connection" option3="No Traffic Logs" option4="Works on All Devicesc"  price="Free" button="Seletion"></PlanesSuscripcion>
        <PlanesSuscripcion imageUrl="./images/logo.svg" title="Free Plan" imgUrl="./images/revisado.svg" option="Unlimited Bandwitc" option2="Encrypted Connection" option3="No Traffic Logs" option4="Works on All Devicesc"  price="$9" button="Seletion"></PlanesSuscripcion>
        <PlanesSuscripcion imageUrl="./images/features-img.svg" title="Free Plan" imgUrl="./images/revisado.svg" option="Unlimited Bandwitc" option2="Encrypted Connection" option3="No Traffic Logs" option4="Works on All Devicesc"  price="$12" button="Seletion"></PlanesSuscripcion>

      </div>
  

      
   
      
    </main>


  );
}




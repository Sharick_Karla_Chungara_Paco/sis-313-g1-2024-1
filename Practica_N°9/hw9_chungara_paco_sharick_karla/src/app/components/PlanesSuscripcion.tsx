export default function PlanesSuscripcion(props:{imageUrl:any,title:String,imgUrl:any,option:String,option2:String, option3:String, option4:String, price:String,button:any }){
    return(
        <div className="planes-div">
              <img className="Planes-imagen" src={props.imageUrl} alt="" />
              <h2 className="Planes-title">{props.title}</h2>
            <div className="Planes-option" >
                   <li className="Planes-lista"><img className="Planes-img"src={props.imgUrl}alt="" />{props.option}</li>
                   <li className="Planes-lista"><img className="Planes-img"src={props.imgUrl}alt="" />{props.option2}</li>
                   <li className="Planes-lista"><img className="Planes-img"src={props.imgUrl}alt="" />{props.option3}</li>
                   <li className="Planes-lista"><img className="Planes-img"src={props.imgUrl}alt="" />{props.option4}</li>
            </div>
                <h1 className="Planes-price">{props.price}<span>/ mo</span></h1>
                <button className="Botoncito">{props.button}</button>
        </div>

    )
}
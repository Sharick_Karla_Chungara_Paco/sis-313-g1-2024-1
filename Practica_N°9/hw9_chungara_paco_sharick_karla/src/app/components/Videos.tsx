export default function Videos(props:{title:String,type:String ,videosUrl:any}){
    return(
        <div className="Square">
            <h1 className="Square-title">{props.title}</h1>
            <p className="Square-type">{props.type}</p>
            <video  src={props.videosUrl} controls className="Square-video"></video>
        </div>
        
    )
}
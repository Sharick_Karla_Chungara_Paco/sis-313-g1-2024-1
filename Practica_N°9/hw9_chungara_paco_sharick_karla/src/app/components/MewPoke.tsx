export default function MewPoke(props:{title:String,imageUrl:any,type:String }){
    return(
        <div className="Banner">
            <h1 className=" Banner-title">{props.title}</h1>
            
            <img className="Banner-imagen"src={props.imageUrl}alt=""></img>
            
            <p className="Banner-type">type: {props.type}</p>
        </div>
        
    )
}

# 🍂 **PRÁCTICA N° 10** 🍂
🍃🎓**Univ.:** Sharick Karla Chungara Paco🍃

🍃👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa🍃 

🍃👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 🍃

🍃🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA🍃

🍃📚**Siglas:** SIS-313 G1 🍃

🍃🗓️**Fecha:** 07/05/2024🍃

 # 🍁 LINKS:🍁
 # 🌱Primer Template🌱

    🧡🍂🧡🍂🧡🍂🧡🍂 Link del template 🧡🍂🧡🍂🧡🍂🧡🍂
                      😸CLICK EN EL MISHI😸

[![🥒🌱🥬](https://github.com/sharick-14/imgenes-prc4/assets/164090219/74327347-c0a0-427d-82e5-76bf8e2ed19b)](https://freebiesbug.com/figma-freebies/travel-agency-template/)

    

    🧡🍂🧡🍂Link del proyecto en Figma utilizando el template🧡🍂🧡🍂
                       😸CLICK EN EL MISHI😸

[![descarga (4)](https://github.com/sharick-14/imgenes-prc4/assets/164090219/15c0eba9-0c52-49a6-b87b-db6608591bcc)](https://www.figma.com/design/lRWd1IbSggNCjSUEmPMPcN/Tour-Guide---travel-agency%2Ftravel-booking-website-(Community)?node-id=37-2&t=HCzbeMUYK0pSG0ek-0)


 # 🌱Segundo Template🌱

    🧡🍂🧡🍂🧡🍂🧡🍂 Link del template 🧡🍂🧡🍂🧡🍂🧡🍂
                        😸CLICK EN EL MISHI😸
[![BTS y TXT](https://github.com/sharick-14/imgenes-prc4/assets/164090219/4ab34f58-1242-4a49-bca8-67a7094748ae)](https://freebiesbug.com/figma-freebies/tech-landing-page/)

    

    🧡🍂🧡🍂Link del proyecto en Figma utilizando el template🧡🍂🧡🍂
                          😸CLICK EN EL MISHI😸
[![CAT_33](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2c7bc195-cab5-4583-89aa-6ddb5be452d3)](https://www.figma.com/file/vo07mA7gML5WPZ5NKwW19O/Virtual-Headset-Landing-Page-UI-FREEBIE-(Community)?type=design&node-id=0-1&mode=design&t=3wugKRmg6mCuyu5P-0)




    🪻🏵️🪻🏵️🪻🏵️Link del repositorio creado🪻🏵️🪻🏵️🪻🏵️
        😸CLICK EN EL RATONCITO para ir al repositorio😸


   [![descarga (7)](https://github.com/sharick-14/imgenes-prc4/assets/164090219/69dffc07-9f2e-4743-9cdb-2252564b851c)](https://gitlab.com/Sharick_Karla_Chungara_Paco/react_with-sharick_karla_chungara_paco)
 


                        TAREA COMPLETA 🌟🌟🌟🌟🌟


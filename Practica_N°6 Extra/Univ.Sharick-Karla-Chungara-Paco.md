
# **PRÁCTICA EXTRA** 📓
🎓**Univ.:** Sharick Karla Chungara Paco 

👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa 

👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

📚**Siglas:** SIS-313 G1 

🗓️**Fecha:** 05/04/2024

# ***Cuenta de Figma***

![cuenta_de_Figma](https://github.com/sharick-14/imgenes-prc4/assets/164090219/05c106e4-eccb-4f63-af5b-e563a8899c54)


# ***Repositorio para el Parcial 1°***

![repositorio-parcial1](https://github.com/sharick-14/imgenes-prc4/assets/164090219/b609aefc-4d99-4e05-9668-849347c8a6f8)